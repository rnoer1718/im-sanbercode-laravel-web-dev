<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\CastController;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class, 'home']);
Route::get('/form', [AuthController::class, 'form']);
Route::post('/index', [AuthController::class, 'kirim']);

Route::get('/data-table', function () {
    return view('page.data-table');
});

Route::get('/table', function () {
    return view('page.table');
});

//testing
// Route::get('/master', function () {
//     return view('layout.master');
// });

//CRUD
//Create
Route::get('/cast/create', [CastController::class, 'create']);
//kirim data ke daatabase
Route::post('/cast', [CastController::class, 'store']);
//Read
Route::get('/cast', [CastController::class, 'index']);
//Detail
Route::get('/cast/{cast_id}', [CastController::class, 'show']);
//Update
Route::get('/cast/{cast_id}/edit', [CastController::class, 'edit']);
//kirim simpan data ke database
Route::put('/cast/{cast_id}', [CastController::class, 'update']);
//Delete
Route::delete('/cast/{cast_id}', [CastController::class, 'destroy']);
