@extends('layout.master')

@section('title')
Buat Account Baru!
@endsection

@section('content')
<h3>Sign Up Form</h3>
<form action="/index" method="post">
    @csrf
    <label>Firstname :</label><br><br>
    <input type="text" name="firstname"><br><br>
    <label>Lastname :</label><br><br>
    <input type="text" name="lastname"><br><br>
    <label>Gender :</label><br><br>
    <input type="radio">Male<br>
    <input type="radio">Female<br>
    <input type="radio">Other<br><br>
    <label>Nationality :</label><br><br>
    <select name="nationality">
        <option value="indonesian">Indonesian</option>
        <option value="japan">Japan</option>   
    </select><br><br>
    <label>Nationality :</label><br><br>
    <input type="checkbox" name="indonesia">Bahasa Indonesia <br>
    <input type="checkbox" name="english">English <br>
    <input type="checkbox" name="other">Other <br><br>
    <label>Bio :</label><br><br>
    <textarea name="bio" rows="10" cols="30"></textarea>
   <br><br>
    <button type="submit">Sign Up</button>
</form>
@endsection
        
