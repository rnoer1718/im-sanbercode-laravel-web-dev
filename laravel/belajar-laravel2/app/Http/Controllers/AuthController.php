<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Symfony\Component\Console\Input\Input;

class AuthController extends Controller
{
    public function form()
    {
        return view('page.form');
    }

    public function kirim(Request $request)
    {
        $namaDepan = $request->input('firstname');
        $namaBelakang = $request->input('lastname');

        return view('page.index', ['namaDepan' => $namaDepan, 'namaBelakang' => $namaBelakang]);
    }
}
