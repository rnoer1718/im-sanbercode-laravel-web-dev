@extends('layout.master')

@section('title')
Halaman List Cast
@endsection

@section('content')
<a href="/cast/create" class="btn btn-primary btn-sm mb-3">Tambah Data</a>
<table class="table">
    <thead>
      <tr>
        <th scope="col">No</th>
        <th scope="col">Nama</th>
        <th scope="col">Umur</th>
        <th scope="col">Action</th>
      </tr>
    </thead>
    <tbody>
        @forelse ($cast as $key => $row)
        <tr>
            <td>{{$key + 1}}</td>
            <td>{{$row->nama}}</td>
            <td>{{$row->umur}}</td>
            <td>
                <form action="/cast/{{$row->id}}" method="POST">
                    @csrf
                    @method('DELETE')
                    <a href="/cast/{{$row->id}}" class="btn btn-info btn-sm">Detail</a>
                    <a href="/cast/{{$row->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
                    <input type="submit"  value="Delete" class="btn btn-danger btn-sm">
                </form>
            </td>
        </tr>
        @empty
            <tr>
                <td>Tidak Ada Data</td>
            </tr>
        @endforelse
    </tbody>
  </table>
@endsection